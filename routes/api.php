<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SaleController;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {
    Route::prefix('item')->group(function () {
        Route::get('/', [ItemController::class, 'itemList']);
        Route::get('/{id}/edit', [ItemController::class, 'getItem']);
        Route::post('/create', [ItemController::class, 'createItem']);
        Route::delete('/delete/{id}', [ItemController::class, 'deleteItem']);
    });

    Route::prefix('customer')->group(function () {
        Route::get('/', [CustomerController::class, 'CustomerList']);
        Route::get('/{id}/edit', [CustomerController::class, 'getCustomer']);
        Route::post('/create', [CustomerController::class, 'createCustomer']);
        Route::delete('/delete/{id}', [CustomerController::class, 'deleteCustomer']);
    });

    Route::prefix('sale')->group(function () {
        Route::get('/', [SaleController::class, 'SaleList']);
        Route::get('/{id}/edit', [SaleController::class, 'getSale']);
        Route::post('/create', [SaleController::class, 'createSale']);
        Route::delete('/delete/{id}', [SaleController::class, 'deleteSale']);
    });

});
