import AllItem from './components/ItemComponent.vue';
import CreateProduct from './components/CreateProduct.vue';
import EditProduct from './components/EditProduct.vue';
import CustomerList from './components/CustomerList.vue';
import CustomerCreate from './components/CreateCustomer.vue';
import CustomerEdit from './components/EditCustomer.vue';
import Sale from './components/Sale.vue';
import SaleList from './components/SaleList.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Sale
    },

    {
        name: 'sale-list',
        path: '/sale/list',
        component: SaleList
    },

    {
        name: 'item-list',
        path: '/item/list',
        component: AllItem
    },

    {
        name: 'create-item',
        path: '/item/create',
        component: CreateProduct
    },
    {
        name: 'edit-item',
        path: '/item/edit/:id',
        component: EditProduct
    },

    {
        name: 'customer-list',
        path: '/customer/list',
        component: CustomerList
    },

    {
        name: 'customer-create',
        path: '/customer/create',
        component: CustomerCreate
    },
    {
        name: 'customer-edit',
        path: '/customer/edit/:id',
        component: CustomerEdit
    },





];
