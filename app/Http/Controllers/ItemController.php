<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    public function itemList(Request $request)
    {
        $data = Item::query();
        if ($request->nama_item) {
            $exp = explode(' ', $request->nama_item);
            foreach ($exp as $value) {
                $data->whereRaw(
                    '
                        (LOWER(`name`) like ? )
                    ',
                    ['%' . $value . '%']
                );
            }
        }
        $data = $data->paginate(20);
        $res = $data->map(function ($q) {
            return [
                'id' => $q->id,
                'nama_item' => $q->nama_item,
                'unit' => $q->unit,
                'stok' => $q->stok,
                'harga_satuan' => $q->harga_satuan,
                'barang' => url('storage/'. $q->barang),
            ];
        });
        $list['next_page'] = $data->hasMorePages();
        $list['total_item'] = $data->total();
        $list['current_page'] = $data->currentPage();
        $list['page_count'] = $data->lastPage();


        return response()->json([
            'code' => 200,
            'message' => 'success',
            'success' => true,
            'data' => [
                'list' => $res,
                'page' => $list
            ]
        ]);
    }

    public function createItem(Request $request)
    {
        $rules = [
            'nama_item' => 'required',
            'unit' => 'required|in:kg,pcs',
            'stok' => 'required|numeric',
            'harga_satuan' => 'required|numeric',
        ];

        if (empty($request->id)) {
            $rules['barang'] = 'required|mimes:jpg,jpeg,png,svg';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'message' => 'error',
                'data' => $validator->errors()->all()
            ]);
        }

        $save = Item::query()->findOrNew($request->id);
        $save->nama_item = $request->nama_item;
        $save->unit = $request->unit;
        $save->slug = Str::slug($request->nama_item);
        $save->stok = $request->stok;
        $save->harga_satuan = $request->harga_satuan;
        $uploaded = null;
        if ($request->hasFile('barang')) {
            $file = $request->file('barang');
            $path  = ('public/uploads/image');
            $filename = time() . '_' . $file->getClientOriginalName();
            $uploaded = $file->storeAs($path, $filename);
            $save->barang = 'uploads/image/' . $filename;
        }
        $save->save();

        return response()->json([
            'code' => 200,
            'success' => true,

            'message' => 'success add item',

        ]);
    }

    public function getItem($slug)
    {
        $records = Item::query()->find($slug);

        if (!$records) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'item not found',
            ]);
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $records
        ]);
    }

    public function deleteItem($id)
    {
        $records = Item::query()->find($id);
        if (!$records) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'item not found',
            ]);
        }

        $records->delete();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'item deleted'
        ]);
    }
}
