<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{
    public function SaleList(Request $request)
    {
        $data = Sale::query()->with('customer');
        if ($request->nama_Sale) {
            $exp = explode(' ', $request->nama_Sale);
            foreach ($exp as $value) {
                $data->whereRaw(
                    '
                        (LOWER(`nama`) like ? )
                    ',
                    ['%' . $value . '%']
                );
            }
        }
        $data = $data->paginate(20);
        $res = $data->map(function ($q) {
            return [
                'id' => $q->id,
                'code_trx' => $q->code_trx,
                'tgl_trx' => $q->tgl_trx,
                'customer' => $q->customer->nama,
                'item' => $q->item,
                'qty' => $q->qty,
                'total_diskon' => $q->total_diskon,
                'total_harga' => $q->total_harga,
                'total_bayar' => $q->total_bayar,
            ];
        });
        $list['next_page'] = $data->hasMorePages();
        $list['total_Sale'] = $data->total();
        $list['current_page'] = $data->currentPage();
        $list['page_count'] = $data->lastPage();


        return response()->json([
            'code' => 200,
            'message' => 'success',
            'success' => true,

            'data' => [
                'list' => $res,
                'page' => $list
            ]
        ]);
    }

    public function createSale(Request $request)
    {
        $rules = [
            'customer_id' => 'required',
            'item' => 'required',
            'qty' => 'required',
            'total_diskon' => 'required',
            'total_harga' => 'required',
            'total_bayar' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'message' => 'error',
                'data' => $validator->errors()->all()
            ]);
        }

        $save = Sale::query()->findOrNew($request->id);
        $save->tgl_trx = Carbon::now();
        $save->customer_id = $request->customer_id;
        $save->item = json_encode($request->item);
        $save->qty = $request->qty;
        $save->total_diskon = $request->total_diskon;
        $save->total_harga = $request->total_harga;
        $save->total_bayar = $request->total_bayar;
        $save->save();

        $save->code_trx = 'T.' . date('Ym') . sprintf('%06s', $save->id);
        $save->save();

        foreach ($request->item as $item) {
            $items = Item::query()->find($item['product_id']);
            $items->stok = $items->stok - $item['qty'];
            $items->save();
        }
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'success add Sale',

        ]);
    }

    public function getSale($slug)
    {
        $records = Sale::query()->find($slug);

        if (!$records) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Sale not found',
            ]);
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $records
        ]);
    }

    public function deleteSale(Request $request)
    {
        $records = Sale::query()->find($request->id);
        if (!$records) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Sale not found',
            ]);
        }

        $records->delete();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Sale deleted'
        ]);
    }
}
