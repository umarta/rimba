<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function CustomerList(Request $request)
    {
        $data = Customer::query();
        if ($request->nama_Customer) {
            $exp = explode(' ', $request->nama_Customer);
            foreach ($exp as $value) {
                $data->whereRaw(
                    '
                        (LOWER(`nama`) like ? )
                    ',
                    ['%' . $value . '%']
                );
            }
        }
        $data = $data->paginate(20);
        $res = $data->map(function ($q) {
            return [
                'id' => $q->id,
                'nama' => $q->nama,
                'contact' => $q->contact,
                'email' => $q->email,
                'alamat' => $q->alamat,
                'diskon' => $q->diskon,
                'ktp' => url($q->ktp),
                'tipe_diskon' => $q->tipe_diskon,
            ];
        });
        $list['next_page'] = $data->hasMorePages();
        $list['total_Customer'] = $data->total();
        $list['current_page'] = $data->currentPage();
        $list['page_count'] = $data->lastPage();


        return response()->json([
            'code' => 200,
            'message' => 'success',
            'success' => true,

            'data' => [
                'list' => $res,
                'page' => $list
            ]
        ]);
    }

    public function createCustomer(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'alamat' => 'required|string',
            'ktp' => 'required|mimes:jpg,jpeg,png,svg',
            'diskon' => 'required',
            'tipe_diskon' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'message' => 'error',
                'data' => $validator->errors()->all()
            ]);
        }

        $save = Customer::query()->findOrNew($request->id);
        $save->nama = $request->nama;
        $save->contact = $request->contact;
        $save->email = $request->email;
        $save->alamat = $request->alamat;
        $save->diskon = $request->diskon;
        $save->tipe_diskon = $request->tipe_diskon;
        $uploaded = null;
        if ($request->hasFile('ktp')) {
            $file = $request->file('ktp');
            $path  = ('public/uploads/ktp');
            $filename = time() . '_' . $file->getClientOriginalName();
            $uploaded = $file->storeAs($path, $filename);

            $save->ktp = 'uploads/ktp/' . $filename;
        }

        $save->save();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'success add Customer',

        ]);
    }

    public function getCustomer($slug)
    {
        $records = Customer::query()->find($slug);

        if (!$records) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Customer not found',
            ]);
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $records
        ]);
    }

    public function deleteCustomer($id)
    {
        $records = Customer::query()->find($id);
        if (!$records) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Customer not found',
            ]);
        }

        $records->delete();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Customer deleted'
        ]);
    }
}
